function initializeWebchat(socketurl, title) {
    WebChat.default.init({
        selector: "#webchat",
        embedded: false,
        initPayload: "/get_started",
        customData: { "language": "en" },
        socketUrl: socketurl,
        socketPath: null,
        title: title,
        showFullScreenButton: false,
        hideWhenNotConnected: false,
        docViewer: false,
        params: {
            storage: "session"
        }
    });
}

function reloadWebchatScript() {
    let head = document.getElementsByTagName('head')[0];
    let oldWebchatScript = document.getElementById("webchatScript");
    let webchatScriptSrc = oldWebchatScript.src;
    head.removeChild(oldWebchatScript);
    let newWebchatScript = document.createElement("script");
    newWebchatScript.src = webchatScriptSrc;
    newWebchatScript.id = "webchatScript";
    head.appendChild(newWebchatScript);
}

window.addEventListener('message', event => {
    const config = event.data;
    reloadWebchatScript();
    initializeWebchat(config.socketUrl, config.title);
    WebChat.open();
});
