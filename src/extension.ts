import * as vscode from 'vscode';
import { ChatbotClientViewProvider } from './providers/ChatbotClientViewProvider';

export function activate(context: vscode.ExtensionContext) {

	console.log('rasa-vscode-integration is now active!');

	const provider = new ChatbotClientViewProvider(context.extensionUri);

	context.subscriptions.push(
		vscode.window.registerWebviewViewProvider(
			ChatbotClientViewProvider.viewType, 
			provider,
			{webviewOptions : {retainContextWhenHidden:true}}
		)
	);
}

export function deactivate() {}
