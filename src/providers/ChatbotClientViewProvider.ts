import * as vscode from "vscode";

export class ChatbotClientViewProvider implements vscode.WebviewViewProvider
{
    public static readonly viewType = 'chatbotClient';

	private _view?: vscode.WebviewView;

	constructor(
		private readonly _extensionUri: vscode.Uri,
    ) { }
    
    public resolveWebviewView(
		webviewView: vscode.WebviewView,
		context: vscode.WebviewViewResolveContext,
		_token: vscode.CancellationToken,
	) {
		this._view = webviewView;

		webviewView.webview.options = {
			enableScripts: true,
			localResourceRoots: [
				this._extensionUri
			]
		};

		webviewView.webview.html = this.getHtmlForWebview(webviewView.webview);

		webviewView.webview.postMessage(this.getConfig());

		vscode.workspace.onDidChangeConfiguration(event => {
			if (event.affectsConfiguration("rasaChatbotIntegration.title")) {
				webviewView.webview.postMessage(this.getConfig());
			}
			if (event.affectsConfiguration("rasaChatbotIntegration.socketUrl")) {
				
				webviewView.webview.postMessage(this.getConfig());
			}
		});
    }
    
    private getHtmlForWebview(webview: vscode.Webview) {
		
		const webchatUri = webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'resources', 'webchat.js'));
		const initChatWidgetUri = webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'resources', 'initChatWidget.js'));
		console.log(initChatWidgetUri);

		const styleChatWidgetUri = webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'resources', 'chatWidget.css'));

		return `<!DOCTYPE html>
            <html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<script id="webchatScript" src="${webchatUri}"></script>
				<link href="${styleChatWidgetUri}" rel="stylesheet">
				<title>Chatbot Client</title>
			</head>
			<body>
				<div id="webchat"/>
				<script id="initScript" src="${initChatWidgetUri}"></script>
			</body>
			</html>`;
    }

	private getConfig() {
		let config = vscode.workspace.getConfiguration("rasaChatbotIntegration");
		console.log(config);
		return config;
	}
}